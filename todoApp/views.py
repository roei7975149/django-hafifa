from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers import serialize
import json

from .serializers import TodoSerializer
from .models import Todo

# Create your views here.

@require_http_methods(["POST"])
@csrf_exempt
def addToDo(request):
    data = json.loads(request.body.decode('utf-8'))
    if data is not None:
        todo = Todo(
            id=data['id'],
            title=data["title"],
            description=data["description"],
            dueDate=data["dueDate"],
            resources=data["resources"]
            )
        todo.save()
        return JsonResponse(f"Task inserted with ObjectId: {todo.id}", safe=False)

@require_http_methods(["PATCH", "DELETE"])
@csrf_exempt
def finishTask(request, id):
    try:
       todo = Todo.objects.get(id=id)
       print(f"Todo object retrieved: {todo}")
       if request.method == "PATCH":
        todo.completed = True
        todo.save()
        message = f"Task {id} marked as {'completed' if todo.completed else 'not completed'}"

       elif request.method == "DELETE":
          todo.delete() 
          message = f"Task {id} deleted succesfuly"

       return JsonResponse({"message": message})
    
    except Todo.DoesNotExist:
        return JsonResponse({"error": f"Task with id {id} not found"}, status=404)
    
    except Exception as e:
        return JsonResponse({"error": f"Error: {e}"}, status=500)
    
@require_http_methods(["GET"])
@csrf_exempt
def getAllTasks(request):
   try:
      todos = Todo.objects.all()
      todos = json.loads(serialize("json", todos))
      todosData = []
      for todo in todos:
        todosData.append(todo["fields"])
      return JsonResponse({"todos": todosData})
   
   except Exception as e:
        return JsonResponse({"error": f"Error: {e}"}, status=500)
   
@require_http_methods(["GET"])
@csrf_exempt
def getNotCompleted(request):
   try:
      todos = Todo.objects.filter(completed__in=[False])
      todos = json.loads(serialize("json", todos))
      todosData = []
      for todo in todos:
        todosData.append(todo["fields"])
      return JsonResponse({"todos": todosData})
   
   except Exception as e:
        return JsonResponse({"error": f"Error: {e}"}, status=500)
    