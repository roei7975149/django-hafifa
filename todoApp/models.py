from django.db import models

# Create your models here.

class Todo(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    dueDate = models.DateTimeField()
    resources = models.TextField()
    completed = models.BooleanField(default=False)