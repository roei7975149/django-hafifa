from django.urls import path
from . import views

urlpatterns = [
    path('addTask/', views.addToDo, name='addTask'),
    path('tasks/<id>', views.finishTask, name='finishTask'),
    path('allTasks/', views.getAllTasks, name='allTasks'),
    path("notCompleted/", views.getNotCompleted, name='notCompleted'),
]